## Git
- make log nice formatted: `git config format.pretty oneline`
- male log colorful: `git config color.ui true`
- list all commits history at current branch: `git log --oneline --abbrev-commit --all <--graph> <--decorate> <--color>`
- add `alias gg='git log --oneline --abbrev-commit --all --graph --decorate --color'` or `git config --global alias.graph "log --graph --oneline –decorate=short"`


## Misc
- Change default location of screencapture:
    `defaults write com.apple.screencapture location /path/to/screenshots/folder`
    `killall SystemUIServer`

