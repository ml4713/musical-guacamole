### Git Essentials

- `git status -s`
- `.gitignore`: [git ignore examples](https://github.com/github/gitignore)
- `git commit -a -m "commit msg"`: skip add step and automatcally stage every file that is already tracked
- `git mv file1 file2`: equals to `mv file1 file2` and then `git rm file1` and then `git add file2`
- `git commit --amend`: if you commit and then realize you forgot to stage the changes in a file you wanted to add to this commit you can do `git commit -m "init commit"` and then `git add forgotten_line` and then `git commit --amend`
- `git checkout -- filename.md`: unmodifying a modified file


### Git remotes
To be able to collaborate on any git project, you need to know how to manage your remote repos. Remote repos are versions of your project that are hosted on the internet or network somewhere. 

- `git remote add pb https://github.com/paulboone/ticgit`: add new remote git named it as `pb`
- `git fetch pb`: fetch all the information that paul has but that you don't yet have in your repo.
- `git pull`: fetches data from the server you originally cloned from and automatically tries to merge it into the code you're currently working on.
- `git remote show pb`: see more info about a particular remote
- `git remote rename pb paul`: rename and removing remotes
- `git config remote.origin.fetch "+refs/heads/svr_plot_chnage:refs/remotes/origin/svr_plot_chnage"`


### Git Taggings
Usually use this to mark release points like v1.0 or v1.1

### Git Aliases
Set up an alias for each command using `git config`. 
- `git config --global alias.ci commit`: instead of typing `git commit` you just need to type `git ci`
- `git config --global alias.unstage 'reset HEAD --'`: to correct the usability problem you encountered with unstaging a file, you can add your own unstage alias to Git. Therefore, instead of type `git reset HEAD -- fileName`, you can just do `git unstage fileName`
- `git config --global alias.last 'log -l HEAD'`: type `git last` to check the last commit


### Git branching
Branching means you diverge from the main line of development and continue to do work without messing with that main line. 

- `git checkout -b test_brn`: create a branch named testing branch and switch to that branch
- `git merge test_brn`: merge the test_brn back into your master branch
