#########
#  Setup#
#########
rm(list = ls())
gc()


list.of.packages <- c("partykit", "rpart.plot", "dummies", "rpart", "data.table", "ggplot2", "stringr", "compare", "gtools", "BAMMtools", "plyr")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[, "Package"])]

if (length(new.packages)) sapply(new.packages, install.packages)

sapply(list.of.packages, require, character.only = T)

set.seed(1015)

source("code/min_split_tune_func.R")
source("code/write_output_func.R")
source("code/get_features_func.R")
source("code/statsSummaryFunc.R")

cut.fun <- function(x) as.character(cut(x, breaks = brk, labels = c("3", "2", "1"), include.lowest = T))
#########
#  Data #
#########

dat <- readRDS("data/ws2-1_tree_data.rds")
dat[, `Final Category Number` := NULL]

all.variables <- fread("data/exploration_var.csv")
dnb.variables <- fread("data/DNB_var.csv")
focused.ind <- fread("data/ws2-1-4_tree_focused_industry.csv")
cntry <- fread("data/cntry_ranked_by_gpw.csv")
acct.products <- fread("data/major acct tech products 2016.csv")
setkey(acct.products, "DNB_NO")
acct.products[is.na(acct.products)] <- 0
dat <- dat[acct.products, nomatch = 0]
products <- colnames(acct.products)[!colnames(acct.products) %in% c("DNB_NO", "Total")]
tree.box.colors <- fread("data/tree_box_colors.csv", colClasses = c("numeric", "character"))

newFCN <- readRDS("data/ws2-1_tree_data_newFCN.rds")
setkey(newFCN, "Ult D&B#")

dat <- dat[newFCN, nomatch = 0]
no.classes <- length(unique(dat$`Final Category Number`))
